void tijd() {
  //time
  //dateTime = NTPch.getNTPtime(1.0, 1); from NTP server
  dateTime = NTPch.getTime(1.0, 1); // get time from internal clock
  if (dateTime.valid) {
    NTPch.printDateTime(dateTime);
    actualHour = dateTime.hour;
    actualMinute = dateTime.minute;
    actualsecond = dateTime.second;
    actualyear = dateTime.year;
    actualMonth = dateTime.month;
    actualday = dateTime.day;
    actualdayofWeek = dateTime.dayofWeek;
  }
  String payload="{\"Uur\":"+ String(actualHour)+",\"Min\":"+String(actualMinute)+",\"Mo\":"+String(actualMonth)+",\"Jaar\":"+String(actualyear)+",\"Dag\":"+String(actualday)+",\"Dow\":"+String(actualdayofWeek)+"}";
  payload.toCharArray(data, (payload.length() + 1));

  client.publish("home/HTU21/Tijd", data);
  //client.publish("home/DHT/jaar", String(actualyear).c_str());
  //client.publish("home/DHT/DOW", String(actualdayofWeek).c_str());
}
