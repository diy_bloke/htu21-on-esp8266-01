#include <ESP8266WiFi.h>        // Include   Wi-Fi library
#include <ESP8266WiFiMulti.h>   // this one is used to allow connection to more than one Router
#include <PubSubClient.h>  // the one from knolleary
#include "userdata.h"
#include <Wire.h>

//For HTU21D
#include "SparkFunHTU21D.h"
HTU21D myHumidity;

WiFiClient espClient;  //voor de MQTT verbinding
PubSubClient client(espClient);//voor de MQTT verbinding
IPAddress MQTT_SERVER_IP;

ESP8266WiFiMulti wifiMulti;     // Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'

const char* ssid1     = "TPLINK-1";         // The SSID (name) of the Wi-Fi network you want to connect to
const char* password1 = "PW1";     // The password of the Wi-Fi network
const char* ssid2 = "TPLINK-2";
const char* password2 = "PW22";
String naam = (__FILE__);     // filenaam

void setup() {
  bestandsnaam(); // this willgive you the name of the file
  Serial.begin(115200);         // Start the Serial communication to send messages to the computer
  delay(10);
  Serial.println('\n');
  wifi_station_set_hostname("HTU21");
  // ---------------- Deze sectie bevat alles wat nodig is voor WiFi verbinding------------------
  wifiMulti.addAP(ssid1, password1);   // add Wi-Fi networks you want to connect to
  wifiMulti.addAP(ssid2, password2);
  Serial.print("Connecting to ");

  int i = 0;
  while (wifiMulti.run() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
    delay(1000);
    Serial.print(++i); Serial.print(' ');
  }
  //----------------- Einde WiFi sectie ---------------------------------------------------------

  Serial.println('\n');
  Serial.println("Connection established!");
  Serial.println(WiFi.SSID());
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());         // Send the IP address of the ESP8266 to the computer
  //-----------------------
  //get MQTT servers address if it is on a known domain name, otherise just use known IP
  WiFi.hostByName("192.168.1.103", MQTT_SERVER_IP);

  Serial.print("MQTT_SERVER_IP: ");
  Serial.println(MQTT_SERVER_IP);

  //Establish conenction to MQTT server
  client.setServer(MQTT_SERVER_IP , MQTT_SERVER_PORT);
  client.setCallback(callback);
  Serial.println("MQTT connected");
  //Phew, have a bit of a breather to give everything a chance to settle down
  delay(1000);
  
  //-------------
  Wire.begin(SDA, SCL); //(0,2);
  myHumidity.begin();
  //for SNTP
  while (!NTPch.setSNTPtime());
}

void loop() {
  if (!client.connected()) {
    if (fDebug) {
      Serial.println("Reconnect");
    }
    reconnect();
  }

  //----periodic update every minute
  currenttime = millis();
  if (currenttime - starttime > 60000)
  {
    starttime = millis();
    client.publish("home/HTU21/naam", naam.c_str());

    sensor();
    tijd();
  }
 
  client.loop();

}


void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    if (fDebug) {
      Serial.print("INFO: Attempting MQTT connection...");
    }
    // Attempt to connect
    if (client.connect(MQTT_CLIENT_ID, MQTT_USER, MQTT_PASSWORD)) {
      if (fDebug) {
        Serial.println("INFO: connected");
      }
      // Once connected, publish an announcement...
      // publishLightState();
      // ... and resubscribe
      client.subscribe("test");
    } else {
      if (fDebug) {
        Serial.print("ERROR: failed, rc=");
        Serial.print(client.state());
        Serial.println("DEBUG: try again in 5 seconds");
      }
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//The callback is not used
void callback(char* p_topic, byte* p_payload, unsigned int p_length) {}